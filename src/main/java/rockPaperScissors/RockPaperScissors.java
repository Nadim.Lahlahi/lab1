package rockPaperScissors;

import java.util.Random;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {

        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();

    }

    private Scanner sc = new Scanner(System.in);
    private int roundCounter = 1;
    private int humanScore = 0;
    private int computerScore = 0;
    private List<String> rpsChoices = Arrays.asList("rock", "paper", "scissor");

    public void run() {

        while (true) {
            System.out.println("Let´s play round " + roundCounter);
            String playermove;

            Random r = new Random();
            int randomNumber = r.nextInt(rpsChoices.size());
            String computermove = rpsChoices.get(randomNumber);

            while (true) {
                playermove = readInput("Your choice (Rock/Paper/Scissor)? ");
                playermove = playermove.toLowerCase();
                
                if (playermove.equals("rock") || playermove.equals("paper") || playermove.equals("scissor")) {
                    break;
                }
                else {
                    System.out.println("I do not understand " + playermove + ". Could you try again? ");
                    {
                        
                    }
                }
            }

            //
            checkResult(playermove, computermove);
           
           
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            // Spør om brukeren vil spille en runde til
            String more_games = readInput("Do you wish to continue playing? (y/n)? ");
            if (more_games.equals("y")) {
                roundCounter++;
                continue;
            }

            else if (more_games.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
        }
    }

    /**
     * uerguierbugeubir
     * 
     * @param playermove
     * @param computermove
     */
    private void checkResult(String playermove, String computermove){
        if (playermove.equals(computermove)) {
            System.out.println("Human chose " + playermove + ", computer chose " + computermove + ". It´s a tie!");
        }

        else if (playermove.equals("rock") && computermove.equals("paper")) {
                System.out.println(
                        "Human chose " + playermove + ", computer chose " + computermove + ". Computer wins!");
                computerScore++;
        } 
        else if (playermove.equals("rock") && computermove.equals("scissor")) {
                System.out.println(
                        "Human chose " + playermove + ", computer chose " + computermove + ". Human wins!");
                humanScore++;
            
        } else if (playermove.equals("paper") && computermove.equals("rock")) {
            if (computermove.equals("rock")) {
                System.out.println(
                        "Human chose " + playermove + ", computer chose " + computermove + ". Human wins!");
                humanScore++;
            } else if (computermove.equals("scissor")) {
                System.out.println(
                        "Human chose " + playermove + ", computer chose " + computermove + ". Computer wins!");
                computerScore++;
            }
        } else if (playermove.equals("scissor")) {
            if (computermove.equals("paper")) {
                System.out.println(
                        "Human chose " + playermove + ", computer chose " + computermove + ". Human wins! ");
                humanScore++;
            } else if (computermove.equals("rock")) {
                System.out.println(
                        "Human chose " + playermove + ", computer chose " + computermove + ". Computer wins!");
                computerScore++;
            }
        }

    }

    
    /**
     * Reads input from console with given prompt
     * 
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
